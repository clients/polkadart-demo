// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blockchain_listener.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$blockchainListenerHash() =>
    r'ca7eb490cba93c9aecf9c83162711454e72f985d';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$BlockchainListener extends BuildlessNotifier<BalanceState> {
  late final String address;

  BalanceState build(
    String address,
  );
}

/// See also [BlockchainListener].
@ProviderFor(BlockchainListener)
const blockchainListenerProvider = BlockchainListenerFamily();

/// See also [BlockchainListener].
class BlockchainListenerFamily extends Family<BalanceState> {
  /// See also [BlockchainListener].
  const BlockchainListenerFamily();

  /// See also [BlockchainListener].
  BlockchainListenerProvider call(
    String address,
  ) {
    return BlockchainListenerProvider(
      address,
    );
  }

  @override
  BlockchainListenerProvider getProviderOverride(
    covariant BlockchainListenerProvider provider,
  ) {
    return call(
      provider.address,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'blockchainListenerProvider';
}

/// See also [BlockchainListener].
class BlockchainListenerProvider
    extends NotifierProviderImpl<BlockchainListener, BalanceState> {
  /// See also [BlockchainListener].
  BlockchainListenerProvider(
    String address,
  ) : this._internal(
          () => BlockchainListener()..address = address,
          from: blockchainListenerProvider,
          name: r'blockchainListenerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$blockchainListenerHash,
          dependencies: BlockchainListenerFamily._dependencies,
          allTransitiveDependencies:
              BlockchainListenerFamily._allTransitiveDependencies,
          address: address,
        );

  BlockchainListenerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.address,
  }) : super.internal();

  final String address;

  @override
  BalanceState runNotifierBuild(
    covariant BlockchainListener notifier,
  ) {
    return notifier.build(
      address,
    );
  }

  @override
  Override overrideWith(BlockchainListener Function() create) {
    return ProviderOverride(
      origin: this,
      override: BlockchainListenerProvider._internal(
        () => create()..address = address,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        address: address,
      ),
    );
  }

  @override
  NotifierProviderElement<BlockchainListener, BalanceState> createElement() {
    return _BlockchainListenerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BlockchainListenerProvider && other.address == address;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, address.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BlockchainListenerRef on NotifierProviderRef<BalanceState> {
  /// The parameter `address` of this provider.
  String get address;
}

class _BlockchainListenerProviderElement
    extends NotifierProviderElement<BlockchainListener, BalanceState>
    with BlockchainListenerRef {
  _BlockchainListenerProviderElement(super.provider);

  @override
  String get address => (origin as BlockchainListenerProvider).address;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
