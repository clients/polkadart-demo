import 'dart:async';
import 'package:durt2/durt2.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'transaction_provider.g.dart';

@riverpod
class TransactionManager extends _$TransactionManager {
  late final TransactionService _transactionService;

  @override
  TransactionStatusModel build() {
    _transactionService = TransactionService();
    return TransactionStatusModel.empty();
  }

  Future<void> pay({
    required KeyPair keypair,
    required String dest,
    required double amount,
  }) async {
    final transactionStream = _transactionService.pay(
      keypair: keypair,
      dest: dest,
      amount: amount,
    );

    transactionStream.listen((event) async {
      state = event;
      if (event.state == TransactionState.finalized) {
        await Future.delayed(const Duration(seconds: 3));
        state = TransactionStatusModel.empty();
      }
    });
  }
}
