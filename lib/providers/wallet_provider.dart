import 'package:durt2/durt2.dart';
import 'package:flutter/material.dart';
import 'package:gazelle/providers/account_history_provider.dart';
import 'package:gazelle/providers/blockchain_listener.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'wallet_provider.g.dart';

@Riverpod(keepAlive: true)
class WalletManager extends _$WalletManager {
  final isMnemonicValid = ValueNotifier<bool>(false);

  @override
  WalletData? build() {
    return null;
  }

  Future<void> clearWallet() async {
    await WalletService.clearWallet();
    if (state == null) return;
    ref.read(accountHistoryProvider(state!.address).notifier).dispose();
    state = null;
  }

  Future updateState(String address, [int? safeBoxNumber]) async {
    var walletData = WalletService.getWalletData(address, safeBoxNumber);
    if (walletData == null) {
      walletData = WalletData(
        address: address,
        safeBoxNumber: safeBoxNumber ?? 0,
      );
      await WalletService.storeWalletData(walletData);
    }
    state = walletData;
    ref
        .read(blockchainListenerProvider((address)).notifier)
        .startBalanceListening();
  }

  bool loadDefaultWallet() {
    final walletData = WalletService.getDefaultWalletData();
    state = walletData;
    return state != null;
  }
}
