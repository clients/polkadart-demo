import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/global.dart';
import 'package:gazelle/providers/account_history_provider.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/screens/widgets/import_mnemonic.dart';
import 'package:gazelle/screens/widgets/transaction_item.dart';

class HomeSection extends ConsumerStatefulWidget {
  const HomeSection({super.key});

  @override
  ConsumerState<HomeSection> createState() => _HomeSectionState();
}

class _HomeSectionState extends ConsumerState<HomeSection> {
  final _scrollController = ScrollController();
  Timer? _debounceTimer;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    const duration = Duration(milliseconds: 300);
    final address = ref.read(walletManagerProvider)!.address;
    if (maxScroll - currentScroll <= 100) {
      if (_debounceTimer?.isActive ?? false) {
        _debounceTimer?.cancel();
        _debounceTimer = Timer(duration, () {
          ref.read(accountHistoryProvider(address).notifier).fetchNextPage(20);
        });
      } else {
        ref.read(accountHistoryProvider(address).notifier).fetchNextPage(20);
        _debounceTimer = Timer(duration, () {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final walletState = ref.watch(walletManagerProvider);
    if (walletState?.address == null) {
      return ImportMnemonic();
    } else {
      return _buildHistoryView();
    }
  }

  Widget _buildHistoryView() {
    final walletState = ref.read(walletManagerProvider)!;
    final historyAsync = ref.watch(accountHistoryProvider(walletState.address));
    ref
        .read(accountHistoryProvider(walletState.address).notifier)
        .subscribeToAccountHistory();

    Future<void> _refreshTransactions() async {
      ref
          .read(accountHistoryProvider(walletState.address).notifier)
          .refetchData();
    }

    return historyAsync.when(
      loading: () {
        return const Center(child: CircularProgressIndicator());
      },
      error: (error, stack) {
        log.e('Error: $error');
        return Center(child: Text('Error: $error'));
      },
      data: (transactions) {
        Widget transactionList() {
          if (transactions.isEmpty) {
            return const Center(
                child: Text('No transactions', style: TextStyle(fontSize: 16)));
          }
          return ListView.builder(
            controller: _scrollController,
            itemCount: transactions.length,
            itemBuilder: (context, index) {
              final tx = transactions[index];
              return TransactionItem(tx);
            },
          );
        }

        return RefreshIndicator(
          onRefresh: _refreshTransactions,
          child: transactionList(),
        );
      },
    );
  }
}
