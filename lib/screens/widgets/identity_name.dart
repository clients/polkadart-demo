import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:gazelle/global.dart';
import 'package:gazelle/providers/squid_provider.dart';

class IdentityName extends ConsumerWidget {
  final String address;
  final TextStyle style;

  const IdentityName({
    required this.address,
    this.style = const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
    ),
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final identityNameAsync = ref.watch(identityNameProvider(address));

    return identityNameAsync.map(
      data: _buildIdentityName,
      loading: _buildLoading,
      error: _buildError,
    );
  }

  Widget _buildIdentityName(AsyncData<String?> data) {
    final identityName = data.value;
    return identityName == null ? SizedBox() : Text(identityName, style: style);
  }

  Widget _buildLoading(AsyncLoading<void> data) {
    return Text('...', style: style);
  }

  Widget _buildError(AsyncError<String?> error) {
    log.e('Error: ${error.error}');
    return Text('Error: ${error.error}');
  }
}
