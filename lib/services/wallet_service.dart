import 'package:durt2/durt2.dart' hide WalletService;
import 'package:durt2/durt2.dart' as durt;
import 'package:gazelle/global.dart';
import 'package:gazelle/providers/wallet_provider.dart';
import 'package:gazelle/screens/widgets/pin_code_dialog.dart';

class WalletService {
  static Future<KeyPair?> getKeyPair() async {
    final mnemonic =
        await Future.delayed(Duration.zero, () => showPinCodeDialog());
    if (mnemonic == null) return null;
    return await KeyPair.sr25519.fromMnemonic(mnemonic);
  }

  static Future<void> generateWallet({
    required String mnemonic,
    required int pinCode,
  }) async {
    try {
      final keypair = await KeyPair.sr25519.fromMnemonic(mnemonic);
      // final safeBoxNumber = WalletService.getDefaultSafeBoxNumber();
      await globalRef
          .read(walletManagerProvider.notifier)
          .updateState(keypair.address);
      await durt.WalletService.storeMnemonic(
        address: keypair.address,
        mnemonic: mnemonic,
        pinCode: pinCode,
      );
    } catch (e) {
      log.e('Invalid mnemonic: $e');
    }
  }
}
