import 'package:polkadart_keyring/polkadart_keyring.dart';

bool isAddress(String address) {
  try {
    Keyring().decodeAddress(address);
    return true;
  } catch (e) {
    return false;
  }
}

String getShortAddress(String address) {
  if (address.length <= 13) {
    return address;
  }
  String start = address.substring(0, 7);
  String end = address.substring(address.length - 6);
  return "$start…$end";
}
